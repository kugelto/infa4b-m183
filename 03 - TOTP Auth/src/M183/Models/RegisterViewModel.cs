using System.ComponentModel.DataAnnotations;

namespace M183.Models
{
    public class RegisterViewModel
    {
        public bool FinishedRegistration { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DataType(DataType.Text)]
        public string Username { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}