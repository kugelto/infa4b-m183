﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace M183.Models
{
    public class TotpSetupViewModel
    {
        public SetupState State { get; set; }
        
        public string QRCode { get; set; }
        public bool ShowError { get; set; }


        public string Token { get; set; }
    }

    public enum SetupState
    {
        Setup,
        Successful
    }
}
