﻿using System.ComponentModel.DataAnnotations;

namespace M183.Models
{
    public class TotpViewModel
    {
        [Required]
        public string Token { get; set; }
    }
}