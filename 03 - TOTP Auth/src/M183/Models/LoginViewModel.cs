using System.ComponentModel.DataAnnotations;

namespace M183.Models
{
    public class LoginViewModel
    {
        [Required(AllowEmptyStrings = false)]
        [DataType(DataType.Text)]
        public string Username { get; set; }

        [Required(AllowEmptyStrings = false)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}