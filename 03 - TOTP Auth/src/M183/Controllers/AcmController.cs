using M183.Data;
using M183.Models;
using M183.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Logging;
using System.Text;
using System.Security.Claims;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.Diagnostics;
using TwoFactorAuthNet;
using Microsoft.AspNetCore.Identity.UI.V3.Pages.Internal.Account.Manage;
using Microsoft.AspNetCore.Identity;

namespace M183.Controllers
{
    public class AcmController : Controller
    {
        public static readonly Claim TOTP_AUTH    = new Claim("TOTP_AUTH", "TRUE");
        public static readonly Claim TOTP_VERIFIED = new Claim("TOTP_VERIFIED", "TRUE");

        private readonly ILogger<AcmController> _logger;
        private readonly M183Database _database;
        private readonly TwoFactorAuth _authenticator;

        public AcmController(ILogger<AcmController> logger, M183Database database, TwoFactorAuth authenticator)
        {
            this._logger        = logger;
            this._database      = database;
            this._authenticator = authenticator;
        }


        // GET: /register
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        // POST: /register
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!ModelState.IsValid)
            {
                ViewData["info"] = "Bitte f�llen Sie alle felder aus.";
                return View();
            }

            // gibt es den Benutzernamen schon?
            if (_database.Users.Any(e => e.Username == model.Username))
            {
                ViewData["info"] = "Dieser Benutzer existiert bereits";
                return View();
            }

            // Byte Salat mit Salz 
            byte[] password = Hashing.HashSHA512(model.Password, out byte[] salt);
            

            string secret = _authenticator.CreateSecret();

            var user = _database.Users.Add(new User(){
                Username    = model.Username,
                Password    = password,
                Salt        = salt,
                IsVerified  = false,
                TotpKey     = secret
            });
            

            await _database.SaveChangesAsync();

            await SignIn(user.Entity);



            return View("Setup", new TotpSetupViewModel { QRCode = _authenticator.GetQrCodeImageAsDataUri(model.Username, secret), State = SetupState.Setup });
        }

        [HttpGet]
        public IActionResult Setup()
        {
            if (!User.Identity.IsAuthenticated)
                return View("Login");

            if (User.HasClaim(e => e.Type == TOTP_VERIFIED.Type))
                return View("Totp");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Setup(TotpSetupViewModel model)
        {
            // Wenn der Benutzer nicht angemeldet ist kann er auch nicht sein Token initialisieren.
            if (!User.Identity.IsAuthenticated)
            {
                return View("Login");
            }

            User user = null;
            if (ModelState.IsValid)
            {
                user = GetUser(out int id);
                if (user == null)
                {
                    _logger.LogError("Invalid state: The user is authenticated but there is no account with the id: {0}", id);
                    return View("ERROR", new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
                }

                // Wenn der Benutzer das Token bereits initialisiert wird er auf die Frontpage
                // weitergeleitet.
                if (user.IsVerified)
                {
                    return RedirectToAction("Index", "Home");
                }

                if (_authenticator.VerifyCode(user.TotpKey, model.Token))
                {
                    // wenn das Token korrekt war wird das "IsVerified" Flag gesetzt.
                    // danach wird er auf die Frontpage weitergeleitet.
                    user.IsVerified = true;
                    _database.Users.Update(user);
                    await _database.SaveChangesAsync();

                    AddTotpIdent(TOTP_VERIFIED, TOTP_VERIFIED);


                    return RedirectToAction("Index", "Home");
                }
            }

            // Wenn der Benutzer das Token falsch eingegeben hat wird ein neuer Key
            // generiert und im angezeigt.
            user.TotpKey = _authenticator.CreateSecret();
            _database.Users.Update(user);
            await _database.SaveChangesAsync();

            return View(new TotpSetupViewModel { QRCode = _authenticator.GetQrCodeImageAsDataUri(user.Username, user.TotpKey), State = SetupState.Setup, ShowError = true });
        }

        // GET: /login
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        // POST: /login
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
                return View();

            User user = _database.Users.FirstOrDefault(e => e.Username == model.Username);

            if (user == null)
            {
                return View();
            }
            
            if (!user.IsVerified)
            {
                return View("Setup", new TotpSetupViewModel { QRCode = _authenticator.GetQrCodeImageAsDataUri(model.Username, user.TotpKey), State = SetupState.Setup });
            }



            if (Hashing.HashData(model.Password, user.Salt).SequenceEqual(user.Password))
            {
                await SignIn(user);
                AddTotpIdent(TOTP_VERIFIED);

                return View("Totp");
            }
            else
            {
                ViewData["info"] = "Es konnte kein Benutzer mit den angegebenen Daten gefunden werden.";
                return View();
            }
        }

        [HttpPost]
        public IActionResult VerifyTotp(TotpViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Totp");
            }

            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Register", "Acm");
            }

            User user = GetUser(out _);
            if (_authenticator.VerifyCode(user.TotpKey, model.Token))
            {
                AddTotpIdent(TOTP_AUTH);
                return RedirectToAction("Index", "Home");
            }
            ViewData["info"] = "Das angegebene Token war leider falsch.";
            return View("Totp");
        }


        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                await HttpContext.SignOutAsync();
            }
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Helper methode um den Benutzer anzumelden.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task SignIn(User user)
        {
            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Username)
            }, CookieAuthenticationDefaults.AuthenticationScheme);

            await HttpContext.SignOutAsync();
            await HttpContext.SignInAsync(new ClaimsPrincipal(identity), new AuthenticationProperties
            {
                IsPersistent = false,
                ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(5),
                AllowRefresh = true,
            });
        }

        private void AddTotpIdent(params Claim[] claims)
        {
            var identity = new ClaimsIdentity(claims , "TOTP");
            User.AddIdentity(identity);
        }

        private User GetUser(out int id)
        {
            int _id = id = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);
            return _database.Users.FirstOrDefault(e => e.Id == _id);
        }
    }
}