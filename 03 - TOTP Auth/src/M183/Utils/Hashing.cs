using System;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace M183.Utils
{
    public static class Hashing
    {
        public static byte[] HashData(string data, in byte[] salt)
        {
            return KeyDerivation.Pbkdf2(data, salt, KeyDerivationPrf.HMACSHA512, 10000, 512 / 8);
        }

        public static byte[] HashSHA512(string data, out byte[] salt)
        {
            salt = GetSalt();
            return KeyDerivation.Pbkdf2(
                    password: data,
                    salt: salt,
                    prf: KeyDerivationPrf.HMACSHA512,
                    iterationCount: 10000,
                    numBytesRequested: 512 / 8
                );
        }

        private static byte[] GetSalt()
        {
            byte[] bytes = new byte[128 / 8];
            using (var keyGenerator = RandomNumberGenerator.Create())
            {
                keyGenerator.GetBytes(bytes);
                return bytes;
            }
        }
    }
}