using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace M183.Data
{
    public class M183Database : DbContext
    {
        public M183Database(DbContextOptions options) 
            : base(options)
        { }

        public DbSet<User> Users { get; set; }
    }
}