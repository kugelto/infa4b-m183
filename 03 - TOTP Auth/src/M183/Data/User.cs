using System.ComponentModel.DataAnnotations;

namespace M183.Data
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MinLength(3)]
        public string Username { get; set; }

        [Required]
        [MaxLength(64)]
        public byte[] Password { get; set; }

        [Required]
        [MaxLength(16)]
        public byte[] Salt { get; set; }

        [Required]
        public string TotpKey { get; set; }

        [Required]
        public bool IsVerified { get; set; }
    }
}