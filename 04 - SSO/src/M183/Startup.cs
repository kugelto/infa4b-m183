﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace M183
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddAuthentication("ExternalLogin")
                    .AddCookie("ExternalLogin")
                    .AddGoogle(options => {

                        options.SignInScheme = "ExternalLogin";
                        options.Scope.Add("profile");
                        options.ClientId        = "331350742125-fgrg29p111e3hqj17kv9io23bq3bgnju.apps.googleusercontent.com";
                        options.ClientSecret    = "hQkV0yCcVV1xxXvSxKGZ1ep8";      
                        options.ClaimActions.MapJsonKey("urn:google:picture", "picture", "url");
                    })
                    .AddMicrosoftAccount(options => {
                        options.SignInScheme = "ExternalLogin";
                        options.Scope.Add("profile");
                        options.Scope.Add("offline_access");
                        options.Scope.Add("user.read");
                        options.Scope.Add("openid");
                        options.ClientId        = "cdd6d7b7-1e46-4146-ac6c-52812256a657";
                        options.ClientSecret    = "ji_8NQBLa4]wX7[M8foeBLLGPushDiJ.";
                    });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
