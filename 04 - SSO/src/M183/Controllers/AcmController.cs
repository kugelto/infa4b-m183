using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Google.Apis.Auth;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Google;
using Microsoft.AspNetCore.Authentication.MicrosoftAccount;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace M183.Controllers
{
    public class AcmController : Controller
    {
        private readonly ILogger<AcmController> _logger;


        public AcmController(ILogger<AcmController> logger)
        {
            this._logger        = logger;
        }


        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpGet]
        public IActionResult LoginWithGoogle()
        {
            return Challenge(new AuthenticationProperties { RedirectUri = Url.Action("Account")},
                GoogleDefaults.AuthenticationScheme);
        }

        [HttpGet]
        public IActionResult LoginWithMicrosoft()
        {
            return Challenge(new AuthenticationProperties { RedirectUri = Url.Action("Account") },
                MicrosoftAccountDefaults.AuthenticationScheme);
        }

        [HttpGet]
        public IActionResult Account()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }
            return Unauthorized();
        }

        [HttpGet]
        public IActionResult LoginWithGithub()
        {
            return Challenge(new AuthenticationProperties { RedirectUri = Url.Action("Account")},
                GoogleDefaults.AuthenticationScheme);
        }

        [HttpGet]
        public async ValueTask<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync("ExternalLogin");
            return RedirectToAction("Index", "Home");
        }


    }
}