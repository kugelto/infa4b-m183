# Module 183

Tobias Kugel


## 01 - XSS Keylogger
Enthält einen Keylogger der alles was eingegeben wird an einen Server sendet.

## 02 - Clickjacking
Enthält ein Beispiel einer Clickjacking attacke anhande der EDUZug login Seite.

## 03 - TOTP Auth
Enthält eine ASP.NET Core Applikation bei der man sich registrieren und ein logen kann.
Beim registrieren muss der Benutzer eine Zeitbassierte Zweifaktor Authentifizierung einrichten,
die beim Login dann abgefragt wird.

## 04 - SSO
Enthält eine ASP.NET Core Applikation bei der man sich über Google oder Microsoft anmelden kann.

## 06 - Passwort
Enthält eine Beschreibung wie ich Passwörter abspeichern würde und eine in implementierung in JavaScript,
die visualisiert wie ich es machen würde.

## 07 - Digest
Enthält die Clientseitige implementation einer HTTP Digest Authentifizierung. 
Dazu entält der Ordner ein Beispiel für eine .htaccess und .htpasswd datei.

## 09 - CSRF
Bei dieser Arbeit ging es darum verschiedene CSRF geschütze API's anzugreifen.

## 10 - Informationsquellen
Bei dieser Arbeit haben wir verschiedene Informationsquellen ausgewert. Dazu haben
wir eine Artikel von einer jeweiligen seite gelesen, eine Kurze zusammenfassung
geschrieben, unsere Meinung dazu abgegeben und gesagt an welche Zeilgruppe die Webseite sich richtet. 
