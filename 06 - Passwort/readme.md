## Passwort Anforderungen
Das Passwort des Benutzer sollte folgende Dinge erfüllen:

- Das Passwort muss mindestens einen Gross und Kleinbuchstaben enthalten, eine Zahl und ein Sonderzeichen.
  - Das Passwort sollte mindestens alle diese Kriterien erfüllen da jede dieser Vorausetztungen die Anzahl möglichen der kombinationen erhöht. Dazu verhindern diese Regel dass Passwörter wie z.B. "1234" möglich sind.
  - Die Webseite sieht erkennt folgende Sonderzeichen: $+@#*§&/\\()=?^~{}[]-_.,;:<> . Aber auch andere sind zugelassen.

- Das Passwort muss mindestens acht Zeichen lang sein. Acht Zeichen sollten noch möglich sein sich zu merken. Dazu ist diese länge ziemlich üblich (standard Windows Policy).
   - Dazu ergibt das, wenn man die Regel von oben mit ein bezieht mindestens 87^8 Kombinationen.

- Das Passwort darf nicht den Benutzernamen enthalten
   - Diese Regel sollte die Benutzerfreundlichkeit wenig bis garnicht beinflussen und verhindert schon mal ein paar schwache Passwörter wie z.B. "M.muster1".

## Persistierung 
Nach der eingabe des Passwortes wird dieses erstmals auf das vorhanden sein aller Anforderungen geprüft. Danach wird das Passwort zuerst mit PBKDF2 und SHA-512 10000 mal gehasht. Nach dem Hashen wird das Passwort noch mit AES-CBC verschlüsselt. Als inizialisierungs vektor wird der salt des Users verwendet und als Cryptokey den Pepper. Das Passwort wird mit dem Pepper nicht gehasht sonder verschlüsselt dammit man diesen wechseln kann. Dazu kann ein Angreiffer, sollange er den Pepper nicht hat, nicht viel mit den Passwörtern anfangen. Dazu würde ich in der live Applikation Passwörter die aus der Datenbank kommen gar nicht mehr entschlüsseln sonder nur das Passwort, dass von Benutzer hoch kommt verschlüsseln.


### Datenbank
In der Datenbank werden salt und passwort zusammen mit dem Benutzernamen gespeichert. Salt und Pepper werden dabei als Base64 String gespeichert.



